# Assignment 02

## *High Throughput Sequencing at your fingertips*


### Deadline:

June 27th 2022


### Delivery format:

* English or Portuguese
* PDF


### Delivery method:

E-mail


### Your Task:

For this assignment you will have to:

* Search the bibliography for a research paper that performs population genetics analyses;
* Understand the proposed biological problem;
* Obtain the *raw reads* **OR** the demultiplxed reads used in the original paper;
* Reduce the dataset to something you consider manageable:
    * If your CPU is an old potato and you have low ammouns of RAM available (≤ 8GB), don't go above 2-4M reads;
    * If your CPU is mid-range and you have a decent ammount of RAM avilable (≤ 16GB), don't go above 8M reads;
    * If you are lucky enough to have a high end CPU (anything above a Ryzen 7 2700) and have generous ammounts of RAM avialable (≥ 20GB) you can probably analyse the entire dataset!;
      * *Hint*: `head` is your friend when reducing your dataste
* Reanalyse the obtained data using both a PCA and admixture plots;
    * Compare your results with those in the original paper;
* Make sure your analyses are reproducible;
* Write a report on what you did;


### In detail:

#### Cover

According to EST's rules. Must include *at least*:

* Title
* EST Barreiro logo
* Date
* Student ID
* Curricular Unit name


#### Introduction

A short (1-3 pages) section describing:
  
* The original biological problem;
* The original used methods;
* The conclusions from the original paper;
* The goals of your work;


#### Materials & Methods

A section where you **detail** how your analyses were performed:

* Which methods were used **for each step**;
* Which software (don't forget to include version number) was used;
* Where can the used scripts be found (there is no way to work around it this time. You **must** make the code available);


#### Results

Write about what **your** analyses revealed. Report only **observable facts** here.

* Include all the relevant plots you produced;
* Describe their most important features;


#### Discussion

Use this section to interpret the results in a biological context.

* How do the plots help solve the biological issue?
* How does the pattern they reveal compare to the ones in the original paper?

Optionally, finish with a *conclusion* section if you think it makes sense in your specific case


#### Author contributions

Note down which tasks each of the group members was responsible for.


#### References

Don't just present a list of consulted material. That simply won't suffice anymore.
For this work (and from now on) you must present the citations in text in addition to the reference list. This time the mandatory style is "APA (American Psychological Association) 7th Edition".
See how it is done in Akihito et al. 2016, the paper from your previous assignment or the paper you are currently analysing.
It is highly recommended that you use reference managing software, like [Zotero](https://www.zotero.org/), [Mendeley](https://www.mendeley.com/download-desktop-new/), or [Endnote](https://endnote.com/) (But there are other alternatives).

### Hints:

* The tougher part of this task will be to find a suitable paper with a dataset. If you thought the last one was hard, just wait until you try this one!
* Make your analyses **reproducible** (detail them as much as you can, so that you can fully repeat them 5 years later);
* Include any commands you use;
* Any used scripts (even if they are only for keeping track of commands), **must** be included as appendices (or even better place them in an online repository like [github](https://github.com) or [gitlab](https://gitlab.com));
* This dataset might take several minutes to 24h to analyse (depending on CPU speed and dataset size). **Plan in advance**;
* Did I mention references before? **DO NOT FORGET TO INCLUDE REFERENCES!!** These have, of course, to be included in the main text, like you see in every research paper;
 * When making citations for software, cite the respective paper (when available) instead of the website;
* This analysis is not as straightforward as the ones from the tutorials. **There are bound to be problems and other analyses issues**. Solve them as best you can;
* Do not hesitate to ask any questions you may have via email;


## Be at your 'top game' for this one. I'm counting on you.
